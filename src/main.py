# based on tutorial from https://www.tensorflow.org/beta/tutorials/text/text_generation


from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'

import tensorflow as tf
from typing import Dict, List, Tuple


def split_into_input_target_pairs(text_chunk: str) -> Tuple[str, str]:
    input_text: str = text_chunk[:-1]
    target_text: str = text_chunk[1:]
    return input_text, target_text


def build_model(vocab_size: int, embedding_dim: int, rnn_units: int, batch_size: int) -> tf.keras.Sequential:
    model_layers = [
        tf.keras.layers.Embedding(vocab_size, embedding_dim, batch_input_shape=[batch_size, None]),
        tf.keras.layers.LSTM(rnn_units, return_sequences=True, stateful=True, recurrent_initializer='glorot_uniform'),
        tf.keras.layers.Dense(vocab_size)
    ]

    return tf.keras.Sequential(model_layers)


def loss(labels, logits):
    return tf.keras.losses.sparse_categorical_crossentropy(labels, logits, from_logits=True)


def generate_text(model: tf.keras.Sequential, initial_text):
    num_generate = 1000
    text_generated = []
    temperature = 1.0

    encoded_input = [char2idx[c] for c in initial_text]
    encoded_input = tf.expand_dims(encoded_input, 0)

    model.reset_states()

    for i in range(num_generate):
        predictions = model(encoded_input)
        predictions = tf.squeeze(predictions, 0)
        predictions = predictions / temperature
        predicted_id = tf.random.categorical(predictions, num_samples=1)[-1, 0].numpy()
        encoded_input = tf.expand_dims([predicted_id], 0)
        text_generated.append(idx2char[predicted_id])

    return initial_text + ''.join(text_generated)


if __name__ == '__main__':
    path_to_file: str = tf.keras.utils.get_file(
        'shakespeare.txt',
        'https://storage.googleapis.com/download.tensorflow.org/data/shakespeare.txt')

    text: str = open(path_to_file, 'rb').read().decode(encoding='utf-8')
    vocab: List[str] = sorted(set(text))

    char2idx: Dict[str, int] = {character: counter for counter, character in enumerate(vocab)}
    idx2char: np.ndarray = np.array(vocab)

    text_as_int: np.ndarray = np.array([char2idx[c] for c in text])

    sequence_length: int = 100
    examples_per_epoch: int = len(text_as_int) // sequence_length

    char_dataset: tf.data.Dataset = tf.data.Dataset.from_tensor_slices(text_as_int)
    sequences: tf.data.Dataset = char_dataset.batch(sequence_length + 1, drop_remainder=True)
    dataset: tf.data.Dataset = sequences.map(split_into_input_target_pairs)

    BATCH_SIZE = 64
    BUFFER_SIZE = 10000

    dataset = dataset.shuffle(BUFFER_SIZE).batch(BATCH_SIZE, drop_remainder=True)

    vocab_size = len(vocab)
    embedding_dim = 256  # what is that?
    rnn_units = 1024

    model = build_model(vocab_size, embedding_dim, rnn_units, BATCH_SIZE)

    # for input_example_batch, target_example_batch in dataset.take(1):
    #     example_batch_predictions = model(input_example_batch)
    #     # print(example_batch_predictions.shape)
    #
    #     sampled_indices = tf.random.categorical(example_batch_predictions[0], num_samples=1)
    #     sampled_indices = tf.squeeze(sampled_indices, axis=-1).numpy()
    #     # print(sampled_indices)
    #     # print(''.join(idx2char[sampled_indices]))
    #
    #     example_batch_loss = loss(target_example_batch, example_batch_predictions)
    #     # print(example_batch_predictions.shape)
    #     # print(example_batch_loss.numpy().mean())

    # model.compile(optimizer='adam', loss=loss)
    #
    checkpoint_dir = './training_checkpoints'
    # checkpoint_prefix = os.path.join(checkpoint_dir, 'chkpt_{epoch}')
    #
    # checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_prefix, save_weights_only=True)
    #
    # EPOCHS = 30
    #
    # history = model.fit(dataset, epochs=EPOCHS, callbacks=[checkpoint_callback])

    model = build_model(vocab_size, embedding_dim, rnn_units, batch_size=1)
    model.load_weights(tf.train.latest_checkpoint(checkpoint_dir))
    model.build(tf.TensorShape([1, None]))

    print(generate_text(model, initial_text=u"ROMEO: "))
